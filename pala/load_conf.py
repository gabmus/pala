from pathlib import Path
import yaml
import toml


def get_suff(path: Path) -> str:
    return path.suffix.lower().strip('.')


def load_conf(path: Path) -> dict:
    fsuff = get_suff(path)
    if fsuff == 'yaml':
        with open(path, 'r') as fd:
            return yaml.safe_load(fd.read())
    elif fsuff == 'toml':
        return toml.load(path)
    raise TypeError(f'Config file type {fsuff} not supported')


def save_conf(path: Path, data: dict):
    fsuff = get_suff(path)
    dump = None
    if fsuff == 'yaml':
        dump = yaml.dump
    elif fsuff == 'toml':
        dump = toml.dumps
    else:
        raise TypeError(f'Config file type {fsuff} not supported')
    with open(path, 'w') as fd:
        fd.write(dump(data))
