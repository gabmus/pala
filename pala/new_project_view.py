from gettext import gettext as _
from gi.repository import Gtk, Adw
from pala.avatar_picker_btn import AvatarPickerBtn
from pala.file_chooser import create_file_chooser
from os.path import expanduser
from pathlib import Path


def _pathify(p: str) -> Path:
    return Path(expanduser(p)).absolute()


class NewSiteData:
    def __init__(self, name: str, path: str, logo: str, author: str):
        self.name = name
        self.path = _pathify(path)
        self.logo = _pathify(logo) if logo else None
        self.author = author

    def __repr__(self):
        return (
            'NewSiteData:\n'
            f'    name: `{self.name}`\n    path: `{self.path}`\n'
            f'    logo: `{self.logo}`\n    author: `{self.author}`'
        )


@Gtk.Template(resource_path='/org/gabmus/pala/ui/new_project_view.ui')
class NewProjectView(Adw.Bin):
    __gtype_name__ = 'NewProjectView'
    path_entry = Gtk.Template.Child()
    name_entry = Gtk.Template.Child()
    author_entry = Gtk.Template.Child()
    logo_picker_btn = Gtk.Template.Child()
    toast_overlay = Gtk.Template.Child()
    fullpath_preview_revealer = Gtk.Template.Child()
    fullpath_preview_label = Gtk.Template.Child()

    def __init__(self):
        super().__init__()

    def get_site_data(self) -> NewSiteData:
        return NewSiteData(
            self.name_entry.get_text(),
            self.path_entry.get_text(),
            self.logo_picker_btn.path,
            self.author_entry.get_text()
        )

    def show_error(self, message: str):
        self.toast_overlay.add_toast(
            Adw.Toast(title=message)
        )

    @Gtk.Template.Callback()
    def on_path_picker_btn_clicked(self, *args):

        def on_accept(dialog):
            self.path_entry.set_text(dialog.get_file().get_path())

        self.fc = create_file_chooser(
            _('Select a Folder'), Gtk.FileChooserAction.SELECT_FOLDER, self,
            on_accept
        )

        self.fc.show()

    @Gtk.Template.Callback()
    def on_name_entry_changed(self, *args):
        self.set_fullpath_preview_label()

    @Gtk.Template.Callback()
    def on_path_entry_changed(self, *args):
        self.set_fullpath_preview_label()

    def set_fullpath_preview_label(self):
        name = self.name_entry.get_text()
        path = self.path_entry.get_text()

        if name and path:
            self.fullpath_preview_revealer.set_reveal_child(True)
            self.fullpath_preview_label.set_markup(
                _('Project location: <b>{0}</b>').format(
                    str(_pathify(f'{path}/{name}'))
                )
            )
        else:
            self.fullpath_preview_revealer.set_reveal_child(False)
            self.fullpath_preview_label.set_text('')
