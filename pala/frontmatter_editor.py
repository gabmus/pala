from gettext import gettext as _
from pathlib import Path
from gi.repository import Gtk, Adw, GObject, Pango
from pala.base_preferences import MActionRow
from pala.file_chooser import create_file_chooser
from pala.hugo import HugoSite
from pala.item_obj import ItemObj


class FMListRowEntry(Gtk.Entry):
    __gsignals__ = {
        'item_changed': (
            GObject.SignalFlags.RUN_LAST,
            None,
            (str,)
        )
    }

    def __init__(self, item: ItemObj, key: str, i: int, txt: str):
        self.key = key
        self.i = i
        self.item = item
        super().__init__(
            text=txt,
            margin_top=12, margin_bottom=12, margin_start=12, margin_end=12
        )
        self.connect('changed', self.on_changed)

    def on_changed(self, *args):
        self.item.frontmatter_dict[self.key][self.i] = self.get_text().strip()
        self.emit('item_changed', '')


class FMListRow(Adw.ExpanderRow):
    __gsignals__ = {
        'changed': (
            GObject.SignalFlags.RUN_LAST,
            None,
            (str,)
        )
    }

    def __init__(self, key: str, item: ItemObj):
        self.key = key
        self.item = item
        super().__init__(title=self.key.capitalize())
        self.add_btn = Gtk.Button(
            icon_name='list-add-symbolic', valign=Gtk.Align.CENTER,
            tooltip_text=_('Add')
        )
        self.add_btn.get_style_context().add_class('flat')
        self.add_btn.connect('clicked', self.new_item)
        self.add_action(self.add_btn)
        self.rows = []
        self.populate()

    def empty(self):
        for row in self.rows:
            self.remove(row.get_parent())
        self.rows = []

    def populate(self):
        self.empty()
        for i, txt in enumerate(self.item.frontmatter_dict[self.key]):
            row = FMListRowEntry(self.item, self.key, i, txt)
            row.connect('item_changed', lambda *args: self.emit('changed', ''))
            self.add_row(row)
            self.rows.append(row)

    def new_item(self, *args):
        self.item.frontmatter_dict[self.key].append('')
        self.populate()
        self.emit('item_changed', '')


class FMFileChooserRow(MActionRow):
    __gsignals__ = {
        'changed': (
            GObject.SignalFlags.RUN_LAST,
            None,
            (str,)
        )
    }

    def __init__(
            self, key: str, item: ItemObj, site: HugoSite, is_image: bool
    ):
        self.key = key
        self.is_image = is_image
        self.item = item
        self.site = site
        super().__init__(title=self.key.capitalize())
        self.button = Gtk.Button(
            valign=Gtk.Align.CENTER,
            label=(self.item.frontmatter_dict[self.key] or _('(None)'))
        )
        self.button.get_child().set_ellipsize(Pango.EllipsizeMode.START)
        self.button.get_style_context().add_class('flat')
        self.button.connect('clicked', self.choose_file)
        self.add_suffix(self.button)
        self.set_activatable_widget(self.button)

    def choose_file(self, *args):
        self.fc_image_filter = None
        if self.is_image:
            self.fc_image_filter = Gtk.FileFilter(name=_('Images'))
            self.fc_image_filter.add_mime_type('image/*')

        def on_accept(dialog):
            fpath = self.site.add_item_media(
                self.item, Path(dialog.get_file().get_path())
            )
            fsitepath = '/media/{0}/{1}'.format(
                self.item.path.stem, fpath.name
            )
            self.item.set_param(self.key, fsitepath)
            self.button.set_label(fsitepath)
            self.emit('changed', '')

        self.fc = create_file_chooser(
            _('Select an Image') if self.is_image else _('Select a File'),
            Gtk.FileChooserAction.OPEN, self, on_accept, self.fc_image_filter
        )

        self.fc.show()


@Gtk.Template(resource_path='/org/gabmus/pala/ui/frontmatter_editor.ui')
class FrontmatterEditor(Gtk.Box):
    __gtype_name__ = 'FrontmatterEditor'
    __gsignals__ = {
        'changed': (
            GObject.SignalFlags.RUN_LAST,
            None,
            (str,)
        )
    }
    preferences_group = Gtk.Template.Child()

    def __init__(self):
        super().__init__()
        self.item = None
        self.rows = []

    def empty(self):
        for row in self.rows:
            self.preferences_group.remove(row)
        self.rows = []

    def set_item(self, item: ItemObj, site: HugoSite):
        self.empty()
        self.item = item
        self.site = site
        for k in self.item.frontmatter_dict.keys():
            w = None
            row = None
            param = self.item.frontmatter_dict[k]
            if isinstance(param, bool):
                w = Gtk.Switch(
                    valign=Gtk.Align.CENTER,
                    active=param
                )
                w.param_key = k
                w.connect(
                    'state-set',
                    lambda s, *args: self.item.set_param(
                        s.param_key, s.get_active()
                    ) or self.emit('changed', '')
                )
                row = MActionRow(k.capitalize())
                row.add_suffix(w)
                row.set_activatable_widget(w)
            elif isinstance(param, str):
                row = MActionRow(k.capitalize())
                if k == 'image':
                    row = FMFileChooserRow(k, item, site, True)
                    row.connect(
                        'changed',
                        lambda *args: self.emit('changed', '')
                    )
                else:
                    w = Gtk.Entry(
                        valign=Gtk.Align.CENTER,
                        text=param
                    )
                    w.param_key = k
                    w.connect(
                        'changed',
                        lambda s, *args: self.item.set_param(
                            s.param_key, s.get_text()
                        ) or self.emit('changed', '')
                    )
                    row.add_suffix(w)
            elif isinstance(param, list):
                row = FMListRow(k, self.item)
                row.connect('changed', lambda *args: self.emit('changed', ''))
            else:
                print(f'FrontmatterEditor: unsupported type {type(param)}')
                continue
            self.rows.append(row)
            self.preferences_group.add(row)
