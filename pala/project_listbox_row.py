from gettext import gettext as _
from gi.repository import Gtk
from pala.projects_list_model import ProjectObj
from pala.accel_manager import add_mouse_button_accel, add_longpress_accel
from pala.action_helper import new_action, new_action_group


@Gtk.Template(resource_path='/org/gabmus/pala/ui/project_listbox_row.ui')
class ProjectListboxRow(Gtk.ListBoxRow):
    __gtype_name__ = 'ProjectListboxRow'
    avatar = Gtk.Template.Child()
    name_label = Gtk.Template.Child()
    path_label = Gtk.Template.Child()
    popover = Gtk.Template.Child()

    def __init__(self, proj: ProjectObj):
        super().__init__()
        self.proj = proj

        self.name_label.set_text(self.proj.name)
        self.path_label.set_text(str(self.proj.path))
        self.avatar.set_text(self.proj.name)
        self.avatar.set_path(self.proj.logo)

        new_action_group(self, 'projrow', [
            new_action('delete', self.delete_proj)
        ])

        add_mouse_button_accel(
            self,
            lambda gesture, *args: self.popover.popup()
            if gesture.get_current_button() == 3  # 3 is right click
            else None
        )
        add_longpress_accel(self, lambda *args: self.popover.popup())

    def delete_proj(self, *args):
        dialog = Gtk.MessageDialog(
            transient_for=self.get_root(),
            modal=True,
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.YES_NO,
            text=_(
                'Are you sure you want to delete "{0}"?\n'
                'The entire site and its relative posts will be deleted.'
            ).format(
                self.proj.name
            )
        )

        def on_response(_dialog, res):
            _dialog.close()
            if res == Gtk.ResponseType.YES:
                self.get_root().welcome_screen.delete_project(self.proj.path)

        dialog.connect('response', on_response)
        dialog.present()
