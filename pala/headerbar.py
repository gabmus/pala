from gi.repository import Gtk, GObject
from pala.confManager import ConfManager


@Gtk.Template(resource_path='/org/gabmus/pala/ui/headerbar.ui')
class GHeaderbar(Gtk.WindowHandle):
    __gtype_name__ = 'GHeaderbar'
    headerbar = Gtk.Template.Child()

    def __init__(self):
        super().__init__()
        self.confman = ConfManager()
