from gi.repository import GLib
from pathlib import Path
from subprocess import Popen, PIPE
from typing import List


class HugoServer:
    def __init__(self, path: Path):
        self.path = path
        self.__port = 1313
        self.__proc = None

    def get_address(self) -> str:
        return f'http://localhost:{self.__port}'

#     def __run_on_free_port(self, cb: Callable):
#
#         def af():
#             self.__port = 1313
#             while not self.__port_is_free(self.__port):
#                 self.__port += 1
#             GLib.idle_add(cb)
#
#         Thread(target=af, daemon=True).start()
#
#     def __port_is_free(self, port: int, try_again: bool = True) -> bool:
#         sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#         result = False
#         try:
#             sock.bind(('127.0.0.1', port))
#             result = True
#         except Exception:
#             if try_again:
#                 sleep(1)
#                 return self.__port_is_free(port, False)
#             pass
#         sock.close()  # why 2? apparently closing isn't instantaneous
#         sock.close()
#         return result

    @property
    def start_cmd(self) -> List[str]:
        return [
            'hugo', 'server',
            '-F',  # include posts from the future
            '--disableFastRender=true',
            '--ignoreCache=true',
            '-s', str(self.path),  # point to the site path,
            '-p', str(self.__port)
        ]

    def start(self):
        if self.is_alive():
            return
        self.__proc = Popen(self.start_cmd, stdout=PIPE, stderr=PIPE)
#         def cb():
#             self.__proc = Popen(self.start_cmd, stdout=PIPE, stderr=PIPE)
#
#         self.__run_on_free_port(cb)

    def stop(self):
        if not self.is_alive():
            return
        self.__proc.terminate()
        if self.is_alive():
            self.__proc.kill()
        self.__proc.communicate()
        self.__proc = None

    def restart(self):
        self.stop()
        self.start()

    def is_alive(self) -> bool:
        return (
            self.__proc is not None and  # proc is set
            self.__proc.poll() is None  # proc is running
        )
