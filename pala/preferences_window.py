from gettext import gettext as _
from pala.confManager import ConfManager
from gi.repository import Adw, Gtk, GtkSource, Pango
from pala.base_preferences import (
    MPreferencesPage, MPreferencesGroup, PreferencesComboRow,
    PreferencesToggleRow, MActionRow
)
from typing import Optional


class SourceViewThemeChooserRow(MActionRow):
    def __init__(
            self, title: str, conf_key: str, signal: str,
            subtitle: Optional[str] = None
    ):
        super().__init__(title, subtitle)
        self.confman = ConfManager()
        self.signal = signal
        self.conf_key = conf_key

        self.btn = GtkSource.StyleSchemeChooserButton(valign=Gtk.Align.CENTER)
        self.btn.get_style_context().add_class('flat')
        self.btn.set_style_scheme(
            GtkSource.StyleSchemeManager.get_default().get_scheme(
                self.confman.conf[self.conf_key]
            ) or GtkSource.StyleSchemeManager.get_default().get_scheme(
                'Adwaita'
            )
        )
        self.btn.connect('notify::style-scheme', self.on_style_scheme_changed)
        self.add_suffix(self.btn)
        self.set_activatable_widget(self.btn)

    def on_style_scheme_changed(self, *args):
        self.confman.conf[self.conf_key] = self.btn.get_style_scheme().get_id()
        self.confman.emit(self.signal, '')
        self.confman.save_conf()


class PreferencesFontChooserRow(MActionRow):
    """
    A preference row with a font chooser button
    """
    def __init__(
        self, title: str, conf_key: str, subtitle: Optional[str] = None,
        signal: Optional[str] = None
    ):
        super().__init__(title, subtitle)
        self.confman = ConfManager()
        self.conf_key = conf_key
        self.signal = signal

        self.font_btn = Gtk.FontButton(
            title=self.title, modal=True, use_size=True,
            use_font=True, font=self.confman.conf[self.conf_key],
            valign=Gtk.Align.CENTER
        )
        self.font_btn.connect('font-set', self.on_font_set)
        self.font_btn.get_style_context().add_class('flat')
        self.font_btn.get_first_child(
        ).get_child().get_first_child().set_ellipsize(Pango.EllipsizeMode.END)
        self.add_suffix(self.font_btn)
        self.set_activatable_widget(self.font_btn)

    def on_font_set(self, *args):
        n_font = self.font_btn.get_font()
        self.confman.conf[self.conf_key] = n_font
        self.confman.save_conf()
        if self.signal:
            self.confman.emit(self.signal, '')


class AppearancePreferencesPage(MPreferencesPage):
    def __init__(self):
        super().__init__(
            title=_('Appearance'), icon_name='preferences-other-symbolic',
            pref_groups=[
                MPreferencesGroup(
                    title=_('Global appearance'), rows=[
                        PreferencesToggleRow(
                            title=_('Dark mode'),
                            conf_key='dark_mode',
                            signal='dark_mode_changed'
                        )
                    ]
                ),
                MPreferencesGroup(
                    title=_('Editor appearance'), rows=[
                        PreferencesToggleRow(
                            title=_('Use custom font'),
                            conf_key='editor_font_enabled',
                            signal='editor_font_changed'
                        ),
                        PreferencesFontChooserRow(
                            title=_('Custom font'),
                            conf_key='editor_font',
                            signal='editor_font_changed'
                        ),
                        SourceViewThemeChooserRow(
                            title=_('Color scheme'),
                            conf_key='editor_theme',
                            signal='editor_theme_changed'
                        ),
                        PreferencesToggleRow(
                            title=_('Show grid pattern'),
                            conf_key='editor_grid_pattern',
                            signal='editor_grid_pattern_changed'
                        )
                    ]
                )
            ]
        )


class PreferencesWindow(Adw.PreferencesWindow):
    def __init__(self):
        super().__init__(default_width=360, default_height=600)
        self.pages = [
            AppearancePreferencesPage(),
        ]
        for p in self.pages:
            self.add(p)
