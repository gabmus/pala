from pathlib import Path
from typing import Optional
from gi.repository import Gtk, Gio, GObject

from pala.confManager import ConfManager


class ProjectObj(GObject.Object):
    def __init__(self, name: str, path: Path, logo: Optional[Path] = None):
        super().__init__()
        self.name = name
        self.path = path
        self.logo = logo


class ProjectsListModel(Gtk.SortListModel):
    def __init__(self):
        self.__search_term = ''
        self.confman = ConfManager()

        self.filter = Gtk.CustomFilter()
        self.filter.set_filter_func(self.__filter_func)
        self.sorter = Gtk.CustomSorter()
        self.sorter.set_sort_func(self.__sort_func)
        self.list_store = Gio.ListStore(item_type=ProjectObj)
        self.filter_store = Gtk.FilterListModel(
            model=self.list_store, filter=self.filter
        )
        super().__init__(model=self.filter_store, sorter=self.sorter)
        self.populate()

    def empty(self):
        self.list_store.remove_all()

    def populate(self):
        self.empty()
        for k in self.confman.conf['projects'].keys():
            proj = self.confman.conf['projects'][k]
            proj = ProjectObj(
                proj['name'], Path(proj['path']),
                Path(proj['logo']) if proj['logo'] else None
            )
            self.list_store.append(proj)

    @property
    def search_term(self) -> str:
        return self.__search_term

    @search_term.setter
    def search_term(self, n_val: str):
        self.__search_term = n_val.strip().lower()
        self.invalidate_filter()

    def invalidate_filter(self):
        self.filter.set_filter_func(self.__filter_func)

    def __filter_func(self, proj: ProjectObj, *args) -> bool:
        if not self.__search_term:
            return True
        return self.__search_term in proj.name.strip().lower()

    def __sort_func(self, p1: ProjectObj, p2: ProjectObj, *args) -> int:
        return -1 if p1.name < p2.name else 1
