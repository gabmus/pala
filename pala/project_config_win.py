from enum import Enum, auto
from gettext import gettext as _
from pathlib import Path
from typing import List
from gi.repository import Gtk, Adw, Gdk
from pala.avatar_picker_btn import AvatarPickerBtn
from pala.hugo import HugoSite
from pala.base_preferences import MActionRow, MPreferencesGroup, MPreferencesPage
from pala.dict_recursive_update import recursive_update


class SiteConfItemType(Enum):
    STR = auto()
    INT = auto()
    FLOAT = auto()
    BOOL = auto()
    COLOR = auto()
    LIST = auto()
    DICT = auto()
    FILE = auto()
    IMAGE = auto()
    LOGO = auto()


class SiteConfItem:
    def __init__(
            self, title: str, key: str, itype: SiteConfItemType, default
    ):
        self.title = title
        self.key = key
        self.itype = itype
        self.default = default

    def set_conf_item(self, site: HugoSite, file: str, n_val):
        parts = self.key.split('/')
        updater = {parts.pop(): n_val}
        for part in reversed(parts):
            updater = {part: updater}
        cnf = site.get_config_file(file)
        recursive_update(cnf, updater)
        site.save_config_file(file, cnf)

    def get_current(self, site: HugoSite, file: str):
        res = site.get_config_file(file)
        for part in self.key.split('/'):
            res = res.get(part, {})
        if res == {}:
            res = self.default
        return res

    def create_action_row(self, site: HugoSite, file: str) -> MActionRow:
        row = MActionRow(title=self.title)
        if self.itype == SiteConfItemType.STR:
            w = Gtk.Entry(
                text=self.get_current(site, file),
                valign=Gtk.Align.CENTER
            )
            w.site = site
            w.file = file
            for sig in ('notify::has-focus', 'activate'):
                w.connect(
                    sig, lambda slf, *args:
                        self.set_conf_item(slf.site, slf.file, slf.get_text())
                )
            row.add_suffix(w)
        elif self.itype == SiteConfItemType.INT:
            adj = Gtk.Adjustment(
                lower=0, page_increment=10, step_increment=1, upper=1000,
                value=self.get_current(site, file)
            )
            w = Gtk.SpinButton(
                adjustment=adj, valign=Gtk.Align.CENTER,
                snap_to_ticks=True
            )
            w.site = site
            w.file = file
            w.connect(
                'changed', lambda slf, *args:
                    self.set_conf_item(
                        slf.site, slf.file, slf.get_value_as_int()
                    )
            )
            row.add_suffix(w)
        elif self.itype == SiteConfItemType.FLOAT:
            adj = Gtk.Adjustment(
                lower=0.0, page_increment=1.0, step_increment=0.01,
                upper=10.0, value=self.get_current(site, file)
            )
            w = Gtk.SpinButton(
                adjustment=adj, valign=Gtk.Align.CENTER,
                snap_to_ticks=True
            )
            w.site = site
            w.file = file
            w.connect(
                'changed', lambda slf, *args:
                    self.set_conf_item(
                        slf.site, slf.file, slf.get_value_as_int()
                    )
            )
            row.add_suffix(w)
        elif self.itype == SiteConfItemType.BOOL:
            w = Gtk.Switch(
                valign=Gtk.Align.CENTER,
                active=self.get_current(site, file)
            )
            w.site = site
            w.file = file
            w.connect(
                'state-set', lambda slf, *args:
                    self.set_conf_item(slf.site, slf.file, slf.get_active())
            )
            row.add_suffix(w)
            row.set_activatable_widget(w)
        elif self.itype == SiteConfItemType.COLOR:
            grgba = Gdk.RGBA()
            grgba.parse(self.get_current(site, file))
            w = Gtk.ColorButton(
                valign=Gtk.Align.CENTER, modal=True, use_alpha=False,
                rgba=grgba
            )
            w.site = site
            w.file = file
            w.connect(
                'color-set', lambda slf, *args:
                    self.set_conf_item(
                        slf.site, slf.file, slf.get_rgba().to_string()
                    )
            )
            row.add_suffix(w)
            row.set_activatable_widget(w)
        elif self.itype == SiteConfItemType.FILE:
            w = Gtk.Button(label='TODO')
            row.add_suffix(w)
            row.set_activatable_widget(w)
        elif self.itype == SiteConfItemType.LOGO:
            w = AvatarPickerBtn()
            w.size = 32
            logo = self.get_current(site, file)
            w.path = Path(logo) if logo else None
            w.avatar.site = site
            w.avatar.connect(
                'changed', lambda slf, *args:
                    slf.site.copy_logo(slf.path)
            )
            row.add_suffix(w)
            row.set_activatable_widget(w)
        else:
            raise TypeError(
                f'Cannot create action row for type {self.itype}'
            )
        return row


class ConfigSection:
    def __init__(
            self, name: str, icon_name: str, file: str,
            items: List[SiteConfItem]
    ):
        self.name = name
        self.icon_name = icon_name
        self.file = file
        self.items = items

    def create_preferences_page(self, site: HugoSite) -> MPreferencesPage:
        group = MPreferencesGroup(
            self.name,
            [ci.create_action_row(site, self.file) for ci in self.items]
        )
        page = MPreferencesPage(self.name, [group], self.icon_name)
        return page


FICURINIA_SPEC = [
    ConfigSection(
        name=_('Site Configuration'),
        icon_name='preferences-system-symbolic',
        file='config.toml',
        items=[
            SiteConfItem(
                _('Title'), 'title', SiteConfItemType.STR, ''
            ),
            SiteConfItem(
                _('Logo'), 'params/logo', SiteConfItemType.LOGO, ''
            ),
            SiteConfItem(
                _('Author'), 'params/author', SiteConfItemType.STR, ''
            ),
            SiteConfItem(
                _('Base URL'), 'baseurl', SiteConfItemType.STR, ''
            ),
            SiteConfItem(
                _('Language Code'), 'languageCode',
                SiteConfItemType.STR, 'en'
            ),
            SiteConfItem(
                _('Default Language'), 'defaultContentLanguage',
                SiteConfItemType.STR, 'en'
            ),
            SiteConfItem(
                _('Copyright'), 'copyright', SiteConfItemType.STR, ''
            ),
            SiteConfItem(
                _('Paginate'), 'paginate', SiteConfItemType.INT, 5
            ),
            SiteConfItem(
                _('Summary Length'), 'summaryLength',
                SiteConfItemType.INT, 70
            ),
            SiteConfItem(
                _('Description'), 'params/description',
                SiteConfItemType.STR, ''
            ),
            SiteConfItem(
                _('Show Posts Link'), 'params/showPostsLink',
                SiteConfItemType.BOOL, True
            ),
            SiteConfItem(
                _('Show Tags'), 'params/showTags', SiteConfItemType.BOOL, True
            ),
            SiteConfItem(
                _('Show RSS'), 'params/showRss', SiteConfItemType.BOOL, True
            ),
            SiteConfItem(
                _('Preview Post Images'), 'params/imageInArticlePreview',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Fit Post Preview Images'),
                'params/fitImageInArticlePreview', SiteConfItemType.BOOL,
                False
            ),
            SiteConfItem(
                _('Show Post Summaries'), 'params/articleSummary',
                SiteConfItemType.BOOL, True
            ),
            SiteConfItem(
                _('Main Font'), 'params/fontFamily', SiteConfItemType.STR,
                'JetBrains Mono'
            ),
            SiteConfItem(
                _('Title Font'), 'params/titleFontFamily',
                SiteConfItemType.STR, 'JetBrains Mono'
            ),
            SiteConfItem(
                _('Monospace Font'), 'params/monospaceFontFamily',
                SiteConfItemType.STR, 'JetBrains Mono'
            ),
            SiteConfItem(
                _('Main Font Size Multiplier'),
                'params/mainFontSizeMultiplier', SiteConfItemType.FLOAT, 1.0
            ),
            SiteConfItem(
                _('Title Font Size Multiplier'),
                'params/titleFontSizeMultiplier', SiteConfItemType.FLOAT, 1.0
            ),
            SiteConfItem(
                _('Monospace Font Size Multiplier'),
                'params/monoFontSizeMultiplier', SiteConfItemType.FLOAT, 1.0
            ),
            SiteConfItem(
                _('Paper Style Cards'), 'params/paperCards',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Button Style Tags'), 'params/buttonTags',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Show Tags in Post Preview'), 'params/tagsInArticlePreview',
                SiteConfItemType.BOOL, True
            ),
            SiteConfItem(
                _('Grid View'), 'params/gridView',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Bigger Post Title'), 'params/bigArticleTitle',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                # TODO improve
                _('Navigation Style ("standard" or "circles")'),
                'params/navtype', SiteConfItemType.STR,
                'standard'
            ),
            SiteConfItem(
                _('Shadows'), 'params/enableShadow',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                # TODO improve
                _('Menu Style ("standard" or "buttons")'),
                'params/menuStyle', SiteConfItemType.STR, 'standard'
            ),
            SiteConfItem(
                _('Search'), 'params/enableSearch',
                SiteConfItemType.BOOL, True
            ),
            SiteConfItem(
                _('Show Search Bar on Every Page'),
                'params/searchbarEverywhere', SiteConfItemType.BOOL, True
            ),
            SiteConfItem(
                _('Show Search Link in Menu'), 'params/searchMenuLink',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Hamburger Menu on Mobile'), 'params/mobileHamburgerNav',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Enable Featured Posts'), 'params/enableFeatured',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Show Underline for Title Links'),
                'params/underlineTitleLinks', SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Show "Share on Fediverse" Button'),
                'params/enableShareOnFediverse', SiteConfItemType.BOOL,
                False
            ),
            SiteConfItem(
                _('Sidebar Layout'), 'params/enableSidebarLayout',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Table of Contents Inside the Sidebar'),
                'params/tocInSidebar', SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Force Redirect to Base URL'), 'params/forceRedirect',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Infinite Scrolling Instead of Pagination'),
                'params/infiniteScrolling', SiteConfItemType.BOOL, False
            ),
            # TODO: footer columns
            SiteConfItem(
                _('Show Related Posts'), 'params/enableRelatedArticles',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Number of Related Posts'), 'params/relatedArticlesNum',
                SiteConfItemType.INT, 2
            ),
            SiteConfItem(
                _('Enable Jumbotron'), 'params/enableJumbotron',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Enable Footer Columns'), 'params/enableFooterColumns',
                SiteConfItemType.BOOL, False
            ),
        ]
    ),
    ConfigSection(
        name=_('Colors'),
        icon_name='preferences-color-symbolic',
        file='data/colors.yaml',
        items=[
            SiteConfItem(
                _('Default Color Scheme ("light" or "dark")'),
                'default', SiteConfItemType.STR, 'dark'
            ),
            SiteConfItem(
                _('Respect User Color Scheme Preference'),
                'auto_switch', SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Dark: Background'),
                'dark/bg', SiteConfItemType.COLOR, '#242629'
            ),
            SiteConfItem(
                _('Dark: Highlights Background'),
                'dark/hl_bg', SiteConfItemType.COLOR, '#34363b'
            ),
            SiteConfItem(
                _('Dark: Foreground'),
                'dark/bg', SiteConfItemType.COLOR, '#ffffff'
            ),
            SiteConfItem(
                _('Dark: Dim Foreground'),
                'dark/dim_fg', SiteConfItemType.COLOR, '#bababa'
            ),
            SiteConfItem(
                _('Dark: Stroke'),
                'dark/stroke', SiteConfItemType.COLOR, '#4f4f4f'
            ),
            SiteConfItem(
                _('Dark: Accent'),
                'dark/accent', SiteConfItemType.COLOR, '#db5793'
            ),
            SiteConfItem(
                _('Dark: Sidebar Background'),
                'dark/sidebar/bg', SiteConfItemType.COLOR, '#bababa'
            ),
            SiteConfItem(
                _('Dark: Sidebar Foreground'),
                'dark/sidebar/fg', SiteConfItemType.COLOR, '#bababa'
            ),
            SiteConfItem(
                _('Light: Background'),
                'light/bg', SiteConfItemType.COLOR, '#f5f5f5'
            ),
            SiteConfItem(
                _('Light: Highlights Background'),
                'light/hl_bg', SiteConfItemType.COLOR, '#e6e6e6'
            ),
            SiteConfItem(
                _('Light: Foreground'),
                'light/bg', SiteConfItemType.COLOR, '#262625'
            ),
            SiteConfItem(
                _('Light: Dim Foreground'),
                'light/dim_fg', SiteConfItemType.COLOR, '#40403e'
            ),
            SiteConfItem(
                _('Light: Stroke'),
                'light/stroke', SiteConfItemType.COLOR, '#575754'
            ),
            SiteConfItem(
                _('Light: Accent'),
                'light/accent', SiteConfItemType.COLOR, '#db5793'
            ),
            SiteConfItem(
                _('Light: Sidebar Background'),
                'light/sidebar/bg', SiteConfItemType.COLOR, '#e6e6e6'
            ),
            SiteConfItem(
                _('Light: Sidebar Foreground'),
                'light/sidebar/fg', SiteConfItemType.COLOR, '#121211'
            ),
        ]
    ),
    ConfigSection(
        name=_('Jumbotron'),
        icon_name='user-home-symbolic',
        file='data/jumbotron.yaml',
        items=[
            SiteConfItem(
                _('Title'), 'title', SiteConfItemType.STR, ''
            ),
            SiteConfItem(
                _('Bigger Title'), 'hugeTitle', SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Subtitle'), 'subtitle', SiteConfItemType.STR, ''
            ),
            # SiteConfItem(
            #     _('Image'), 'image', SiteConfItemType.IMAGE, ''
            # ),
            SiteConfItem(
                _(
                    'Image Position '
                    '("left", "right", "top" or "bottom")'
                ), 'title', SiteConfItemType.STR, 'left'
            ),
            # SiteConfItem(
            #     _('Background Image'), 'image', SiteConfItemType.IMAGE, ''
            # ),
            # SiteConfItem(
            #     _('Background Video (default)'), 'backgroundVideo',
            #     SiteConfItemType.FILE, ''
            # ),
            # SiteConfItem(
            #     _('Background Video (MP4)'), 'backgroundVideoMp4',
            #     SiteConfItemType.FILE, ''
            # ),
            # SiteConfItem(
            #     _('Background Video (WebM)'), 'backgroundVideoWebm',
            #     SiteConfItemType.FILE, ''
            # ),
            SiteConfItem(
                _('Video Opacity (0.0 to 1.0)'), 'videoOpacity',
                SiteConfItemType.FLOAT, 1.0
            ),
            SiteConfItem(
                _('Text Shadow'), 'textShadow', SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Fill Screen Vertically'), 'fullscreen',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Show Down Arrow'), 'downArrow',
                SiteConfItemType.BOOL, False
            ),
            SiteConfItem(
                _('Force White Text'), 'whiteText',
                SiteConfItemType.BOOL, False
            ),
        ]
    )
]


class ProjectConfigWin(Adw.PreferencesWindow):
    __gtype_name__ = 'ProjectConfigWin'

    def __init__(self, site: HugoSite):
        self.site = site
        super().__init__(title='Project Configuration')

        for section in FICURINIA_SPEC:
            self.add(section.create_preferences_page(site))
