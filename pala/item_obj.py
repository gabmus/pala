from gi.repository import GObject
from pathlib import Path
from datetime import datetime, timezone
from typing import Tuple
import yaml


LOCAL_TIMEZONE = datetime.now(timezone.utc).astimezone().tzinfo


class ItemObj(GObject.Object):
    __gsignals__ = {
        'title-changed': (
            GObject.SignalFlags.RUN_LAST,
            None,
            (str,)
        ),
        'delete': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (str,)
        )
    }

    def __init__(self, path: Path):
        super().__init__()
        self.set_path(path)

    def reset(self):
        self.set_path(self.path)

    def set_path(self, n_path: Path):
        self.path = n_path
        self.frontmatter_dict, self.markdown = self.__parse()

    @GObject.Property(type=str)
    def title(self) -> str:
        return self.frontmatter_dict.get('title', '')

    @title.setter
    def title(self, n_title: str):
        self.frontmatter_dict['title'] = n_title
        self.emit('title-changed', '')

    @GObject.Property(type=str)
    def text(self) -> str:
        return self.markdown

    @text.setter
    def text(self, n_text: str):
        self.markdown = n_text
        self.__save()

    @GObject.Property
    def date(self) -> datetime:
        return self.frontmatter_dict.get(
            'date', datetime.fromtimestamp(0)
        )

    @date.setter
    def date(self, n_date: datetime):
        self.frontmatter_dict['date'] = n_date
        self.__save()

    def set_param(self, key: str, n_val):
        if key not in self.frontmatter_dict.keys():
            print(f'Key {key} not found in item')
            return
        if key == 'title':
            if not self.title.strip():
                return
            self.title = n_val
            return
        self.frontmatter_dict[key] = n_val

    def __parse(self) -> Tuple[dict, str]:
        fm = ''
        txt = ''
        lines = []
        with open(self.path, 'r') as fd:
            # skipping first line '---'
            lines = fd.readlines()[1:]
        fm_done = False
        for line in lines:
            if fm_done:
                txt += line
            else:
                if line[:3] == '---':
                    fm_done = True
                else:
                    fm += line
        fm_dict = yaml.safe_load(fm)
        if 'date' in fm_dict.keys():
            if fm_dict['date'].tzinfo is None:
                fm_dict['date'] = fm_dict['date'].replace(
                    # trick to get local timezone
                    # https://stackoverflow.com/a/39079819
                    tzinfo=LOCAL_TIMEZONE
                )
        return (fm_dict, txt)

    def __str__(self):
        return (
            '---\n' +
            yaml.dump(self.frontmatter_dict) +
            '\n---\n' +
            self.markdown
        )

    def __save(self):
        with open(self.path, 'w') as fd:
            fd.write(str(self))
