from gettext import gettext as _
from gi.repository import Gtk
from typing import Callable, Optional


def create_file_chooser(
    title: str, action: Gtk.FileChooserAction, parent: Gtk.Widget,
    on_accept: Callable, filter: Optional[Gtk.FileFilter] = None
) -> Gtk.FileChooserDialog:
    fc = Gtk.FileChooserDialog(
        title=title,
        transient_for=parent.get_root(),
        modal=True,
        action=action,
        create_folders=action in (
            Gtk.FileChooserAction.SAVE,
            Gtk.FileChooserAction.SELECT_FOLDER
        ),
        filter=filter
    )
    fc.add_buttons(
        _('Cancel'), Gtk.ResponseType.CANCEL,
        _('Open'), Gtk.ResponseType.ACCEPT,
    )
    fc.get_widget_for_response(
        Gtk.ResponseType.ACCEPT
    ).get_style_context().add_class('suggested-action')

    def cb(dialog, res):
        if res == Gtk.ResponseType.ACCEPT:
            on_accept(dialog)
        dialog.close()

    fc.connect('response', cb)

    return fc
