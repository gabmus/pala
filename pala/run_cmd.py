from subprocess import Popen, PIPE
from typing import Tuple, List


class RunCmdError(Exception):
    def __init__(self, cmd: List[str], returncode: int,
                 stdout: str, stderr: str):
        self.cmd = cmd
        self.returncode = returncode
        self.stdout = stdout
        self.stderr = stderr
        super().__init__(
            f'Error running command: `{self.cmd}`; '
            f'process exited with code `{self.returncode}`\n    '
            f'stdout: `{self.stdout}`\n    '
            f'stderr: `{self.stderr}`'
        )


def run(cmd: List[str]) -> Tuple[str, str]:
    sp = Popen(
        cmd, stdout=PIPE, stderr=PIPE,
        encoding='utf-8'  # no need to .decode()
    )
    out, err = sp.communicate()
    if sp.returncode != 0:
        raise RunCmdError(cmd, sp.returncode, out, err)
    return (out, err)
