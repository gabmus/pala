from gettext import gettext as _
from shutil import rmtree
from pathlib import Path
from gi.repository import Gtk
from pala.new_project_view import NewProjectView, NewSiteData
from pala.confManager import ConfManager
from pala.hugo import HugoSite
from pala.projects_list_model import ProjectObj, ProjectsListModel
from pala.project_listbox_row import ProjectListboxRow


@Gtk.Template(resource_path='/org/gabmus/pala/ui/welcome_screen.ui')
class WelcomeScreen(Gtk.Box):
    __gtype_name__ = 'WelcomeScreen'
    stack = Gtk.Template.Child()
    welcome_page = Gtk.Template.Child()
    normal_page = Gtk.Template.Child()
    new_project_page = Gtk.Template.Child()
    new_project_view = Gtk.Template.Child()
    projects_listbox = Gtk.Template.Child()
    search_entry = Gtk.Template.Child()
    projects_listbox_placeholder = Gtk.Template.Child()
    searchbar = Gtk.Template.Child()
    search_btn = Gtk.Template.Child()
    projects_sw = Gtk.Template.Child()
    projects_stack = Gtk.Template.Child()

    def __init__(self):
        super().__init__()
        self.confman = ConfManager()
        self.set_stack_initial_child()

        self.projects_list_model = ProjectsListModel()
        self.projects_listbox.bind_model(
            self.projects_list_model, self.__create_project_row, None
        )

    def __create_project_row(
            self, proj: ProjectObj, *args
    ) -> ProjectListboxRow:
        row = ProjectListboxRow(proj)
        return row

    def set_stack_initial_child(self):
        if len(self.confman.conf['projects']) > 0:
            self.stack.set_visible_child_name('normal')
        else:
            self.stack.set_visible_child_name('welcome')

    @Gtk.Template.Callback()
    def toggle_search(self, *args):
        self.searchbar.set_search_mode(self.search_btn.get_active())

    @Gtk.Template.Callback()
    def on_search_mode_changed(self, *args):
        self.search_btn.set_active(self.searchbar.get_search_mode())

    @Gtk.Template.Callback()
    def on_new_btn_clicked(self, *args):
        self.stack.set_visible_child_name('new_project')

    @Gtk.Template.Callback()
    def on_import_btn_clicked(self, *args):
        print('import')

    @Gtk.Template.Callback()
    def on_new_project_back_btn_clicked(self, *args):
        self.set_stack_initial_child()

    @Gtk.Template.Callback()
    def on_new_project_create_btn_clicked(self, *args):
        data = self.new_project_view.get_site_data()
        if not data.path.is_dir():
            # TODO: maybe create folder?
            self.new_project_view.show_error(_('Invalid folder'))
            return
        if data.path.joinpath(data.name).exists():
            self.new_project_view.show_error(_('Folder is not empty'))
            return
        self.__create_site(data)

    @Gtk.Template.Callback()
    def on_projects_listbox_row_activated(self, lb, row):
        site = HugoSite.open_project(
            row.proj.name, row.proj.path
        )
        Gtk.Application.get_default().open_project(site)
        self.get_root().close()

    @Gtk.Template.Callback()
    def on_search_entry_changed(self, *args):
        self.projects_list_model.search_term = self.search_entry.get_text()
        self.projects_stack.set_visible_child(
            self.projects_listbox_placeholder
            if self.projects_listbox.get_row_at_index(0) is None
            else self.projects_sw
        )

    def __create_site(self, data: NewSiteData):
        site = HugoSite(data.name, data.path.joinpath(data.name))
        site.create(data.logo)
        Gtk.Application.get_default().open_project(site)
        self.get_root().close()

    def delete_project(self, proj_path: Path):
        # some safeguards I guess
        if not proj_path.is_dir():
            print(f'Error: path `{proj_path}` is not a directory')
            return
        if proj_path == Path.home() or proj_path == Path('/'):
            print('Error: attempting to remove home or root directory')
            return
        if 'config.toml' not in [f.name for f in proj_path.iterdir()]:
            print('Error: path `{proj_path}` doesn\'t seem to be a hugo site')
            return
        rmtree(proj_path)
        self.confman.conf['projects'].pop(str(proj_path))
        self.projects_list_model.populate()
