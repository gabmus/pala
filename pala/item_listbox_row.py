from gettext import gettext as _
from gi.repository import Gtk, GLib
from pala.item_list_store import ItemObj
from datetime import datetime
from pala.action_helper import new_action, new_action_group
from pala.accel_manager import add_mouse_button_accel, add_longpress_accel


@Gtk.Template(resource_path='/org/gabmus/pala/ui/item_listbox_row.ui')
class ItemListboxRow(Gtk.ListBoxRow):
    __gtype_name__ = 'ItemListboxRow'
    title_label = Gtk.Template.Child()
    datetime_label = Gtk.Template.Child()
    popover = Gtk.Template.Child()

    def __init__(self, item: ItemObj):
        super().__init__()
        self.item = item
        self.item.connect(
            'notify::title',
            lambda *args: self.set_title(self.item.title)
        )
        self.set_title(self.item.title)
        self.datetime_label.set_text(self.date_to_string(self.item.date))
        new_action_group(self, 'itemrow', [
            new_action('delete', self.delete_item)
        ])

        add_mouse_button_accel(
            self,
            lambda gesture, *args: self.popover.popup()
            if gesture.get_current_button() == 3  # 3 is right click
            else None
        )
        add_longpress_accel(self, lambda *args: self.popover.popup())

    def set_title(self, title: str):
        self.title_label.set_text(title)

    def date_to_string(self, date: datetime) -> str:
        gtz = GLib.TimeZone.new(date.strftime('%z'))
        return GLib.DateTime(
            gtz,
            date.year,
            date.month,
            date.day,
            date.hour,
            date.minute,
            date.second
        ).to_local().format("%c")

    def delete_item(self, *args):
        dialog = Gtk.MessageDialog(
            transient_for=self.get_root(),
            modal=True,
            message_type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.YES_NO,
            text=_('Are you sure you want to delete "{0}"?').format(
                self.item.title
            )
        )

        def on_response(_dialog, res):
            _dialog.close()
            if res == Gtk.ResponseType.YES:
                self.item.emit('delete', '')
                self.get_root().site.delete_item(self.item.path)

        dialog.connect('response', on_response)
        dialog.present()
