from pathlib import Path
from gi.repository import Gtk, Gio
from pala.item_obj import ItemObj


class ItemListModel(Gtk.SortListModel):
    def __init__(self):
        self.__sorter = Gtk.CustomSorter()
        self.__sorter.set_sort_func(self.__sort_func)
        self.list_store = Gio.ListStore(item_type=ItemObj)
        super().__init__(model=self.list_store, sorter=self.__sorter)

    def __sort_func(self, i1: ItemObj, i2: ItemObj, *args) -> int:
        return -1 if i1.date > i2.date else 1

    def empty(self):
        self.list_store.remove_all()

    def add_item(self, item: ItemObj):
        self.list_store.append(item)

    def remove_item_by_pos(self, pos: int):
        self.list_store.remove(pos)

    def remove_item_by_path(self, path: Path):
        for i in range(self.list_store.get_n_items()):
            item = self.list_store.get_item(i)
            if item.path == path:
                self.list_store.remove(i)
                return
        print(f'ItemListModel: could not find item with path {path}')
