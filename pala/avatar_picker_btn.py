from gettext import gettext as _
from pathlib import Path
from gi.repository import Gtk, Gdk, Adw, GObject, Gio, GLib
from pala.file_chooser import create_file_chooser
from pala.simple_avatar import SimpleAvatar


@Gtk.Template(resource_path='/org/gabmus/pala/ui/avatar_picker_btn.ui')
class AvatarPickerBtn(Gtk.Button):
    __gtype_name__ = 'AvatarPickerBtn'
    avatar = Gtk.Template.Child()

    fc = None
    fc_image_filter = None

    def __init__(self):
        super().__init__()

    @GObject.Property(type=int, default=32)
    def size(self):
        return self.avatar.get_size()

    @size.setter
    def size(self, n_size):
        self.avatar.set_size(n_size)

    @GObject.Property()
    def path(self) -> Path:
        return self.avatar.path

    @path.setter
    def path(self, n_path: Path):
        self.avatar.set_path(n_path)

    @Gtk.Template.Callback()
    def on_logo_picker_btn_clicked(self, *args):
        self.fc_image_filter = Gtk.FileFilter(name=_('Images'))
        self.fc_image_filter.add_mime_type('image/*')

        def on_accept(dialog):
            self.avatar.set_path(Path(dialog.get_file().get_path()))

        self.fc = create_file_chooser(
            _('Select an Image'), Gtk.FileChooserAction.OPEN, self,
            on_accept, self.fc_image_filter
        )

        self.fc.show()
