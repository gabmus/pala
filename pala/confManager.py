from pathlib import Path
from os.path import isfile
from os import environ as Env
import json
from gi.repository import GObject, Gio
from pala.singleton import Singleton


class ConfManagerSignaler(GObject.Object):
    __gsignals__ = {
        'dark_mode_changed': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (str,)
        ),
        'editor_theme_changed': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (str,)
        ),
        'editor_grid_pattern_changed': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (str,)
        ),
        'editor_font_changed': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (str,)
        ),
    }


class ConfManager(metaclass=Singleton):

    _background_color = None

    BASE_SCHEMA = {
        'dark_mode': False,
        'windowsize': {
            'width': 350,
            'height': 650
        },
        # project structure:
        # path: {path, name, logo, windowsize: {width, height}}
        'projects': {},
        'editor_theme': 'Adwaita',
        'editor_grid_pattern': False,
        'editor_font_enabled': False,
        'editor_font': ''
    }

    def __init__(self):
        self.signaler = ConfManagerSignaler()
        self.emit = self.signaler.emit
        self.connect = self.signaler.connect

        self.is_flatpak = isfile('/.flatpak-info')

        self.conf_dir = Path(
            Env.get('XDG_CONFIG_HOME') or f'{Env.get("HOME")}/.config'
        )
        self.cache_home = Path(
            Env.get('XDG_CACHE_HOME') or f'{Env.get("HOME")}/.cache'
        )
        self.cache_path = self.cache_home.joinpath('org.gabmus.pala')
        for p in [
                self.conf_dir,
                self.cache_path,
        ]:
            if not p.is_dir():
                p.mkdir(parents=True)
        self.path = self.conf_dir.joinpath('org.gabmus.pala.json')

        if self.path.is_file():
            try:
                with open(self.path) as fd:
                    self.conf = json.loads(fd.read())
                # verify that the file has all of the schema keys
                for k in ConfManager.BASE_SCHEMA:
                    if k not in self.conf.keys():
                        if isinstance(
                                ConfManager.BASE_SCHEMA[k], (list, dict)
                        ):
                            self.conf[k] = ConfManager.BASE_SCHEMA[k].copy()
                        else:
                            self.conf[k] = ConfManager.BASE_SCHEMA[k]
            except Exception:
                self.conf = ConfManager.BASE_SCHEMA.copy()
                self.save_conf(force_overwrite=True)
        else:
            self.conf = ConfManager.BASE_SCHEMA.copy()
            self.save_conf()

        bl_gsettings = Gio.Settings.new('org.gnome.desktop.wm.preferences')
        bl = bl_gsettings.get_value('button-layout').get_string()
        self.wm_decoration_on_left = (
            'close:' in bl or
            'maximize:' in bl or
            'minimize:' in bl
        )

    def save_conf(self, *args, force_overwrite=False):
        if self.path.is_file() and not force_overwrite:
            with open(self.path, 'r') as fd:
                if json.loads(fd.read()) == self.conf:
                    return
        with open(self.path, 'w') as fd:
            fd.write(json.dumps(self.conf))
