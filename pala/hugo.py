from gettext import gettext as _
from datetime import datetime
from pathlib import Path
from typing import Optional, List
from pala.item_list_store import ItemListModel
from pala.item_obj import ItemObj, LOCAL_TIMEZONE
from pala.run_cmd import run
from git import Repo
from pala.confManager import ConfManager
from pala.hugo_server import HugoServer
from shutil import copyfile
from os import remove
from pala.load_conf import load_conf, save_conf
import re


POST_TEMPLATE = '''---
title: {0}
date: {1}
description: ''
tags: []
image: ''
featured: false
comments: true
showDate: true
norss: false
nosearch: false
toc: false
---'''

FILENAME_CLEANER = re.compile('[^0-9a-zA-Z]+')


class HugoSite:
    def __init__(self, name: str, path: Path):
        self.name = name
        self.path = path
        self.items_dir = self.path.joinpath('content/posts')
        self.media_dir = self.path.joinpath('content/media')
        # initialization implies:
        # - hugo site was created
        # - git repo is inited
        # - site conf has been loaded
        self.is_initialized = False
        self.repo = None
        self.confman = ConfManager()
        self.site_conf = {}
        self.items_store = ItemListModel()
        self.server = HugoServer(self.path)

    @classmethod
    def open_project(
            cls, name: str, path: Path
    ):  # -> HugoSite
        if not path.exists():
            raise FileNotFoundError(
                f'Project with path {path} does not exist'
            )
        site = cls(name, path)
        site.__load_existing_repo()
        site.__load_site_conf()
        site.__load_items()
        site.is_initialized = True
        return site

    def create(self, logo: Optional[Path] = None):
        if self.is_initialized:
            print('Project already initialized')
            return
        if self.path.exists():
            raise FileExistsError(
                'Cannot create new site on top of an existing location'
            )
        out, err = run(['hugo', 'new', 'site', str(self.path)])
        print(out, err)  # TODO: debug, remove
        self.__init_repo()
        self.__load_site_conf()
        self.__add_ficurinia_theme()
        self.site_conf['title'] = self.name
        self.save_site_conf()
        self.is_initialized = True
        self.confman.conf['projects'][
            str(self.path)
        ] = self.__to_confman_dict()
        self.copy_logo(logo)
        self.confman.save_conf()

    def __load_site_conf(self):
        self.site_conf = load_conf(self.path.joinpath('config.toml'))
        from pprint import pprint
        pprint(self.site_conf)
        if 'params' not in self.site_conf.keys():
            self.site_conf['params'] = dict()
        if 'outputs' in self.site_conf.keys():
            if 'home' in self.site_conf['outputs'].keys():
                for v in ('HTML', 'JSON'):
                    if v not in self.site_conf['outputs']['home']:
                        self.site_conf['outputs']['home'].append(v)
            else:
                self.site_conf['outputs']['home'] = ['HTML', 'JSON']
        else:
            self.site_conf['outputs'] = {'home': ['HTML', 'JSON']}

    def save_site_conf(self):
        save_conf(self.path.joinpath('config.toml'), self.site_conf)

    def copy_logo(self, logo: Optional[Path]):
        if logo is None:
            return
        assert(self.site_conf)
        n_logo_path = self.path.joinpath(f'static/logo{logo.suffix}')
        copyfile(logo, n_logo_path)
        self.confman.conf['projects'][
            str(self.path)
        ]['logo'] = str(n_logo_path)
        self.confman.save_conf()
        self.site_conf['params']['logo'] = f'/{n_logo_path.name}'
        self.save_site_conf()
        # TODO: convert/resize to all needed formats

    def __title_to_filename(self, title: str) -> str:
        return FILENAME_CLEANER.sub('_', title) + '.md'

    def __create_item_obj(self, item_path: Path):
        item = ItemObj(item_path)
        item.connect('title-changed', lambda it, *args: self.rename_item(it))
        self.items_store.add_item(item)

    def create_item(self, item_name: str):
        if not self.items_dir.is_dir():
            if self.items_dir.is_file():
                raise TypeError(
                    f'Posts dir path {self.items_dir} is a file, '
                    'it should be a directory'
                )
            self.items_dir.mkdir()
        filename = self.__title_to_filename(item_name)
        item_path = self.items_dir.joinpath(filename)
        if item_path.exists():
            raise FileExistsError(
                f'Item with path {item_path} already exists'
            )
        with open(item_path, 'w') as fd:
            fd.write(POST_TEMPLATE.format(
                item_name, datetime.now(LOCAL_TIMEZONE)
            ))
        self.__create_item_obj(item_path)

    def rename_item(self, item: ItemObj):
        n_filename = self.__title_to_filename(item.title)
        n_item_path = self.items_dir.joinpath(n_filename)
        while n_item_path.exists():
            n_item_path = n_item_path.with_stem(
                n_item_path.stem + '_copy'
            )
        with open(n_item_path, 'w') as fd:
            fd.write(str(item))
        remove(item.path)
        item.set_path(n_item_path)

    def delete_item(self, item_path: Path):
        if not item_path.exists():
            print(f'Trying to delete file {item_path}, but it does not exist')
            return
        remove(item_path)
        self.items_store.remove_item_by_path(item_path)

    def add_item_media(self, item: ItemObj, media: Path) -> Path:
        dest_dir = self.media_dir.joinpath(item.path.stem)
        if dest_dir.is_file():
            raise FileExistsError(f'Error: path `{dest_dir}` is a file')
        if not self.media_dir.is_dir():
            self.media_dir.mkdir()
        if not dest_dir.is_dir():
            dest_dir.mkdir()
        dest = dest_dir.joinpath(
            datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + media.suffix
        )
        copyfile(media, dest)
        return dest

    def __init_repo(self):
        if self.repo is not None:
            print('Git repo already inited')
            return
        self.repo = Repo.init(self.path)

    def __load_existing_repo(self):
        if self.repo is not None:
            print('Git repo already loaded')
            return
        self.repo = Repo(self.path)

    def __add_ficurinia_theme(self):
        self.__add_theme(
            'ficurinia', 'https://gitlab.com/gabmus/hugo-ficurinia.git'
        )

    def __load_items(self):
        self.items_store.empty()
        if not self.items_dir.is_dir():
            return
        for item_path in self.items_dir.iterdir():
            self.__create_item_obj(item_path)

    def __add_theme(self, name: str, git_url: str, branch: str = 'master'):
        assert self.repo is not None
        assert self.site_conf is not None
        dest = self.path.joinpath(f'themes/{name}')
        if dest.exists():
            print(f'Theme with name {name} already exists')
            return
        self.repo.create_submodule(
            name=name, path=dest, url=git_url, branch=branch
        )
        if 'theme' in self.site_conf.keys():
            self.site_conf['theme'].append(name)
        else:
            self.site_conf['theme'] = [name]
        self.save_site_conf()

    def __to_confman_dict(self) -> dict:
        return {
            'name': self.name,
            'path': str(self.path),
            'logo':
                self.site_conf.get('params', {}).get('logo', None)
                if self.site_conf else None,
        }

    def get_config_file(self, file: str) -> dict:
        if file == 'config.toml':
            return self.site_conf
        fpath = self.path.joinpath(file)
        if not fpath.is_file():
            print(f'Requesting config file `{fpath}` but it doesn\'t exist')
            return {}
        return load_conf(fpath)

    def save_config_file(self, file: str, data: dict):
        if file == 'config.toml':
            return self.save_site_conf()
        fpath = self.path.joinpath(file)
        save_conf(fpath, data)
