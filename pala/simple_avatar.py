from pathlib import Path
from typing import Optional
from gi.repository import Adw, GObject, Gio, GLib, Gdk
from threading import Thread


class SimpleAvatar(Adw.Bin):
    __gtype_name__ = 'SimpleAvatar'
    __gsignals__ = {
        'changed': (
            GObject.SignalFlags.RUN_LAST,
            None,
            (str,)
        )
    }
    __path = None
    __size = 32
    __text = None

    def __init__(
            self, path: Optional[Path] = None, size: int = 32,
            text: Optional[str] = None
    ):
        self.avatar = Adw.Avatar(show_initials=True)
        self.set_path(path)
        self.set_size(size)
        self.set_text(text)
        self.set_child(self.avatar)

    @GObject.Property()
    def path(self) -> Optional[Path]:
        return self.__path

    @path.setter
    def path(self, n_path: Path):
        self.set_path(n_path)

    def set_path(self, n_path: Optional[Path]):
        self.__path = n_path
        self.__set_avatar_image()
        self.emit('changed', '')

    @GObject.Property(type=int)
    def size(self) -> int:
        return self.__size

    @size.setter
    def size(self, n_size: int):
        self.set_size(n_size)

    def set_size(self, n_size: int):
        self.__size = n_size
        self.avatar.set_size(self.__size)
        self.__set_avatar_image

    @GObject.Property(type=str)
    def text(self) -> Optional[str]:
        return self.__text

    @text.setter
    def text(self, n_text: Optional[str]):
        self.set_text(n_text)

    def set_text(self, n_text: Optional[str]):
        if n_text is not None:
            self.__text = n_text
            self.avatar.set_text(self.__text)

    def __set_avatar_image(self):
        if not self.__path or not self.__path.is_file():
            return

        gfile = Gio.File.new_for_path(str(self.__path))

        def cb(texture):
            self.avatar.set_custom_image(texture)

        def af():
            try:
                texture = Gdk.Texture.new_from_file(gfile)
                GLib.idle_add(cb, texture)
            except Exception:
                print(
                    'SimpleAvatar: '
                    f'error creating texture for file {self.__path}'
                )

        Thread(target=af, daemon=True).start()
