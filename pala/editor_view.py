from gettext import gettext as _
from gi.repository import Gtk, GtkSource
from pala.hugo import HugoSite
from pala.item_obj import ItemObj
from pala.frontmatter_editor import FrontmatterEditor
from pala.action_helper import new_action, new_action_group
from pala.confManager import ConfManager
from pala.file_chooser import create_file_chooser
from pathlib import Path


@Gtk.Template(resource_path='/org/gabmus/pala/ui/editor_view.ui')
class EditorView(Gtk.Box):
    __gtype_name__ = 'EditorView'
    headerbar = Gtk.Template.Child()
    sourceview = Gtk.Template.Child()
    sourcebuf = Gtk.Template.Child()
    title_label = Gtk.Template.Child()
    save_btn = Gtk.Template.Child()
    editor_actions_menu_btn = Gtk.Template.Child()
    back_btn = Gtk.Template.Child()
    flap = Gtk.Template.Child()
    frontmatter_editor_toggle = Gtk.Template.Child()
    frontmatter_editor = Gtk.Template.Child()
    unsaved_changes_label = Gtk.Template.Child()

    item = None
    site = None

    def __init__(self):
        super().__init__()
        self.sourcebuf.set_language(
            GtkSource.LanguageManager.get_default().get_language('markdown')
        )
        self.confman = ConfManager()
        self.style_provider = None
        self.confman.connect(
            'editor_theme_changed', lambda *args: self.set_theme()
        )
        self.confman.connect(
            'editor_grid_pattern_changed',
            lambda *args: self.set_background_pattern()
        )
        self.confman.connect(
            'editor_font_changed',
            lambda *args: self.set_font()
        )
        self.set_theme()
        self.set_background_pattern()
        self.set_font()
        new_action_group(self, 'editor', [
            new_action('heading1', lambda *args: self.action_heading(1)),
            new_action('heading2', lambda *args: self.action_heading(2)),
            new_action('heading3', lambda *args: self.action_heading(3)),
            new_action('heading4', lambda *args: self.action_heading(4)),
            new_action('heading5', lambda *args: self.action_heading(5)),
            new_action('heading6', lambda *args: self.action_heading(6)),
            new_action('image', lambda *args: self.action_image()),
            new_action(
                'image_file_picker', lambda *args:
                    self.action_image_file_picker()
            ),
            new_action('codeblock', lambda *args: self.action_surround(
                '\n```\n'
            )),
            new_action('quoteblock', lambda *args: self.action_quoteblock()),
            new_action('hr', lambda *args: self.action_hr()),
            new_action('link', lambda *args: self.action_link(False)),
            new_action(
                'bold', lambda *args: self.action_surround('**'),
                '<Control>b'
            ),
            new_action(
                'italic', lambda *args: self.action_surround('_'),
                '<Control>i'
            ),
            new_action('strikethrough', lambda *args: self.action_surround(
                '~~'
            )),
            new_action(
                'code', lambda *args: self.action_surround('`'),
                # '<Control>`'
            ),
        ])

    def set_theme(self):
        self.sourcebuf.set_style_scheme(
            GtkSource.StyleSchemeManager.get_default().get_scheme(
                self.confman.conf['editor_theme']
            ) or GtkSource.StyleSchemeManager.get_default().get_scheme(
                'Adwaita'
            )
        )

    def set_background_pattern(self):
        self.sourceview.set_background_pattern(
            GtkSource.BackgroundPatternType.GRID
            if self.confman.conf['editor_grid_pattern']
            else GtkSource.BackgroundPatternType.NONE
        )

    def set_font(self):
        # unset style
        if self.style_provider is not None:
            self.sourceview.get_style_context().remove_provider(
                self.style_provider
            )
        if not self.confman.conf['editor_font_enabled']:
            return
        font = self.confman.conf['editor_font']
        family = ' '.join(font.split(' ')[:-1])
        size = font.split(' ')[-1]
        self.style_provider = Gtk.CssProvider()
        self.style_provider.load_from_data(
            f'''.sourceview {{
                    font-family: {family}, monospace;
                    font-size: {size}pt;
            }}'''.encode()
        )
        self.sourceview.get_style_context().add_provider(
            self.style_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

    def __get_insert_iter(self) -> Gtk.TextIter:
        return self.sourcebuf.get_iter_at_mark(
            self.sourcebuf.get_mark('insert')
        )

    def __get_select_iter(self) -> Gtk.TextIter:
        self.sourcebuf.get_iter_at_mark(
            self.sourcebuf.get_mark('selection_bound')
        )

    def __get_chars_after_iter(self, iter: Gtk.TextIter, n: int = 1) -> str:
        return self.sourcebuf.get_text(
            iter,
            self.sourcebuf.get_iter_at_offset(
                iter.get_offset() + n
            ),
            True
        )

    def __del_chars_after_iter(self, iter: Gtk.TextIter, n: int = 1) -> str:
        return self.sourcebuf.delete(
            iter,
            self.sourcebuf.get_iter_at_offset(
                iter.get_offset() + n
            )
        )

    def action_hr(self):
        insert_iter = self.__get_insert_iter()
        line_end = self.sourcebuf.get_iter_at_line_offset(
            insert_iter.get_line(), insert_iter.get_chars_in_line()
        )[1]
        self.sourcebuf.place_cursor(line_end)
        self.sourcebuf.insert_at_cursor('\n\n---\n\n')
        self.sourceview.grab_focus()

    def action_heading(self, level: int):
        insert_iter = self.__get_insert_iter()
        line_start = self.sourcebuf.get_iter_at_line_offset(
            insert_iter.get_line(), 0
        )[1]
        nxtc = self.__get_chars_after_iter(line_start)
        if nxtc == '#':
            while True:
                self.__del_chars_after_iter(line_start)
                nxtc = self.__get_chars_after_iter(line_start)
                if nxtc == '\n' or nxtc not in ('#', ' '):
                    break
        if level <= 0:
            return
        level = min(level, 6)
        self.sourcebuf.insert(line_start, ('#' * level) + ' ')
        self.sourceview.grab_focus()

    def action_quoteblock(self):
        insert_iter = self.__get_insert_iter()
        line_start = self.sourcebuf.get_iter_at_line_offset(
            insert_iter.get_line(), 0
        )[1]
        nxtc = self.__get_chars_after_iter(line_start, 2)
        if nxtc == '> ':
            self.__del_chars_after_iter(line_start, 2)
        else:
            self.sourcebuf.insert(line_start, '> ')
        self.sourceview.grab_focus()

    def action_surround(self, chars: str):
        selection_bounds = self.sourcebuf.get_selection_bounds()
        if not selection_bounds:
            self.sourcebuf.insert(self.__get_insert_iter(), chars*2)
            self.sourcebuf.place_cursor(self.sourcebuf.get_iter_at_offset(
                self.__get_insert_iter().get_offset() - len(chars)
            ))
        else:
            end_offset = selection_bounds[1].get_offset()
            self.sourcebuf.insert(selection_bounds[0], chars)
            self.sourcebuf.insert(self.sourcebuf.get_iter_at_offset(
                end_offset + len(chars)
            ), chars)
        self.sourceview.grab_focus()

    def action_image(self):
        self.action_link(True)

    def action_image_file_picker(self):
        self.fc_image_filter = Gtk.FileFilter(name=_('Images'))
        self.fc_image_filter.add_mime_type('image/*')

        def on_accept(dialog):
            assert(self.site)
            assert(self.item)
            media = Path(dialog.get_file().get_path())
            n_media_path = self.site.add_item_media(self.item, media)
            media_sitepath = '/media/{0}/{1}'.format(
                self.item.path.stem, n_media_path.name
            )
            insert_iter = self.__get_insert_iter()
            line_end = self.sourcebuf.get_iter_at_line_offset(
                insert_iter.get_line(), insert_iter.get_chars_in_line()
            )[1]
            self.sourcebuf.place_cursor(line_end)
            self.sourcebuf.insert_at_cursor(f'\n\n![]({media_sitepath})\n\n')
            self.sourceview.grab_focus()

        self.fc = create_file_chooser(
            _('Select an Image'), Gtk.FileChooserAction.OPEN, self,
            on_accept, self.fc_image_filter
        )

        self.fc.show()

    def action_link(self, image: bool = False):
        selection_bounds = self.sourcebuf.get_selection_bounds()
        text_plch = _('text...')
        link_plch = _('link...')
        if not selection_bounds:
            self.sourcebuf.insert(
                self.__get_insert_iter(),
                ('!' if image else '') + f'[{text_plch}]({link_plch})'
            )
        else:
            end_offset = selection_bounds[1].get_offset()
            start_offset = selection_bounds[0].get_offset()
            selected_text = self.sourcebuf.get_text(*selection_bounds, False)
            if 'https://' in selected_text or 'http://' in selected_text:
                self.sourcebuf.insert(
                    selection_bounds[0],
                    ('!' if image else '') + f'[{text_plch}]('
                )
                self.sourcebuf.insert(
                    self.sourcebuf.get_iter_at_offset(
                        end_offset +
                        (1 if image else 0) +
                        len(f'[{text_plch}](')
                    ),
                    ')'
                )
                self.sourcebuf.place_cursor(
                    self.sourcebuf.get_iter_at_offset(
                        start_offset + (1 if image else 0) +
                        len(f'[{text_plch}')
                    )
                )
            else:
                self.sourcebuf.insert(
                    selection_bounds[0],
                    ('!' if image else '') + '['
                )
                self.sourcebuf.insert(
                    self.sourcebuf.get_iter_at_offset(
                        end_offset + (1 if image else 0) + 1
                    ),
                    f']({link_plch})'
                )
                self.sourcebuf.place_cursor(
                    self.sourcebuf.get_iter_at_offset(
                        end_offset + + (1 if image else 0) + 1 +
                        len(f']({link_plch}')
                    )
                )
        self.sourceview.grab_focus()

    def close_item(self):
        self.item = None
        self.title_label.set_label('')
        self.sourcebuf.set_text('')
        self.sourceview.set_sensitive(False)
        self.save_btn.set_sensitive(False)
        self.frontmatter_editor_toggle.set_sensitive(False)
        self.editor_actions_menu_btn.set_sensitive(False)
        self.flap.set_reveal_flap(False)
        self.frontmatter_editor.empty()
        self.unsaved_changes_label.set_visible(False)

    def open_item(self, item: ItemObj, site: HugoSite):
        self.item = item
        self.site = site
        self.item.reset()
        self.item.connect(
            'notify::title',
            lambda *args: self.title_label.set_label(self.item.title)
        )
        self.item.connect('delete', lambda *args: self.close_item())
        self.title_label.set_label(self.item.title)
        self.sourcebuf.set_text(self.item.text)
        self.sourceview.set_sensitive(True)
        self.save_btn.set_sensitive(True)
        self.frontmatter_editor_toggle.set_sensitive(True)
        self.editor_actions_menu_btn.set_sensitive(True)
        self.flap.set_reveal_flap(False)
        self.frontmatter_editor.set_item(self.item, self.site)
        self.unsaved_changes_label.set_visible(False)

    @Gtk.Template.Callback()
    def on_save_btn_clicked(self, *args):
        assert(self.item is not None)
        self.item.text = self.sourcebuf.get_text(
            self.sourcebuf.get_start_iter(),
            self.sourcebuf.get_end_iter(),
            True
        )
        self.unsaved_changes_label.set_visible(False)

    @Gtk.Template.Callback()
    def on_frontmatter_editor_toggle_toggled(self, *args):
        self.flap.set_reveal_flap(self.frontmatter_editor_toggle.get_active())

    @Gtk.Template.Callback()
    def on_flap_reveal_flap(self, *args):
        self.frontmatter_editor_toggle.set_active(self.flap.get_reveal_flap())

    @Gtk.Template.Callback()
    def on_item_changed(self, *args):
        self.unsaved_changes_label.set_visible(True)

    def set_show_back_btn(self, state: bool):
        self.back_btn.set_visible(state)
