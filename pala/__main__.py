import sys
# import argparse
from gi.repository import Gtk, Gio
from pala.confManager import ConfManager
from pala.app_window import AppWindow
from pala.hugo import HugoSite
from pala.preferences_window import PreferencesWindow
from pala.base_app import BaseApp, AppAction
from pala.project_window import ProjectWindow


class GApplication(BaseApp):
    def __init__(self):
        super().__init__(
            app_id='org.gabmus.pala',
            app_name='Pala',
            app_actions=[
                AppAction(
                    name='preferences',
                    func=self.show_preferences_window,
                    accel='<Primary>comma'
                ),
                AppAction(
                    name='shortcuts',
                    func=self.show_shortcuts_window,
                    accel='<Primary>question'
                ),
                AppAction(
                  name='about',
                  func=self.show_about_dialog
                ),
                AppAction(
                    name='quit',
                    func=lambda *args: self.quit(),
                    accel='<Primary>q'
                ),
                AppAction(
                    name='open_welcome_win',
                    func=lambda *args: self.open_welcome_win(),
                    # accel='<Primary>o'
                )
            ],
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
            css_resource='/org/gabmus/pala/ui/gtk_style.css'
        )
        self.confman = ConfManager()

    def quit(self, *args):
        self.confman.save_conf()
        super().quit()

    def show_about_dialog(self, *args):
        dialog = Gtk.Builder.new_from_resource(
            '/org/gabmus/pala/aboutdialog.ui'
        ).get_object('aboutdialog')
        dialog.set_modal(True)
        dialog.set_transient_for(self.get_active_window())
        dialog.present()

    def show_shortcuts_window(self, *args):
        shortcuts_win = Gtk.Builder.new_from_resource(
            '/org/gabmus/pala/ui/shortcutsWindow.ui'
        ).get_object('shortcuts-win')
        shortcuts_win.props.section_name = 'shortcuts'
        shortcuts_win.set_transient_for(self.get_active_window())
        shortcuts_win.set_modal(True)
        shortcuts_win.present()

    def show_preferences_window(self, *args):
        preferences_win = PreferencesWindow()
        preferences_win.set_transient_for(self.get_active_window())
        preferences_win.set_modal(True)
        preferences_win.present()

    def do_activate(self):
        super().do_activate()
        win = AppWindow()
        self.add_window(win)
        win.present()
        if hasattr(self, 'args'):
            if self.args:
                pass

    def open_welcome_win(self):
        win = AppWindow()
        self.add_window(win)
        win.present()

    def do_command_line(self, args):
        """
        GTK.Application command line handler
        called if Gio.ApplicationFlags.HANDLES_COMMAND_LINE is set.
        must call the self.do_activate() to get the application up and running.
        """
        # call the default commandline handler
        Gtk.Application.do_command_line(self, args)
        # make a command line parser
        #  #parser = argparse.ArgumentParser()
        #  #parser.add_argument(
        #  #    'argurl',
        #  #    metavar=_('url'),
        #  #    type=str,
        #  #    nargs='?',
        #  #    help=_('opml file local url or rss remote url to import')
        #  #)
        # parse the command line stored in args,
        # but skip the first element (the filename)
        #  #self.args = parser.parse_args(args.get_arguments()[1:])
        # call the main program do_activate() to start up the app
        self.do_activate()
        return 0

    def open_project(self, site: HugoSite):
        proj_win = ProjectWindow(site)
        self.add_window(proj_win)
        proj_win.present()


def main():

    application = GApplication()

    try:
        ret = application.run(sys.argv)
    except SystemExit as e:
        ret = e.code

    sys.exit(ret)


if __name__ == '__main__':
    main()
