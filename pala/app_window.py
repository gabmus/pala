# from gi.repository import Adw, GLib
from pala.confManager import ConfManager
from pala.headerbar import GHeaderbar
from pala.base_app import BaseWindow, AppShortcut
from pala.welcome_screen import WelcomeScreen


class AppWindow(BaseWindow):
    def __init__(self):
        super().__init__(
            app_name='Pala',
            icon_name='org.gabmus.pala',
            shortcuts=[AppShortcut(
                'F10', lambda *args: self.headerbar.menu_btn.popup()
            )]
        )
        self.confman = ConfManager()

        # self.headerbar = GHeaderbar()
        # self.append(self.headerbar)

        self.welcome_screen = WelcomeScreen()
        self.append(self.welcome_screen)

        self.confman.connect(
            'dark_mode_changed',
            lambda *args: self.set_dark_mode(self.confman.conf['dark_mode'])
        )
        self.set_dark_mode(self.confman.conf['dark_mode'])
        self.connect('close-request', self.on_destroy)

    def present(self):
        super().present()
        self.set_default_size(
            self.confman.conf['windowsize']['width'],
            self.confman.conf['windowsize']['height']
        )

    def on_destroy(self, *args):
        self.confman.conf['windowsize'] = {
            'width': self.get_width(),
            'height': self.get_height()
        }
        self.confman.save_conf()
