from gettext import gettext as _
from gi.repository import Gtk, Gio
from pala.hugo import HugoSite
from pala.action_helper import new_action, new_action_group


@Gtk.Template(resource_path='/org/gabmus/pala/ui/server_controller_button.ui')
class ServerControllerButton(Gtk.MenuButton):
    __gtype_name__ = 'ServerControllerButton'
    state_label = Gtk.Template.Child()

    def __init__(self):
        self.site = None
        super().__init__()
        self.action_start = new_action('start', self.on_start)
        self.action_stop = new_action('stop', self.on_stop)
        self.action_open_preview = new_action('open_preview', self.on_open_preview)
        self.action_restart = new_action('restart', self.on_restart)
        new_action_group(self, 'server', [
            self.action_start, self.action_stop, self.action_open_preview,
            self.action_restart
        ])

    def set_site(self, site: HugoSite):
        self.site = site

    @Gtk.Template.Callback()
    def on_activate(self, *args):
        if not self.site:
            return
        sc = self.state_label.get_style_context()
        for cl in ('error', 'success'):
            sc.remove_class(cl)
        if self.site.server.is_alive():
            sc.add_class('success')
            self.state_label.set_text(_('State: Active'))
            self.action_start.set_enabled(False)
            self.action_stop.set_enabled(True)
            self.action_open_preview.set_enabled(True)
        else:
            sc.add_class('error')
            self.state_label.set_text(_('State: Inactive'))
            self.action_start.set_enabled(True)
            self.action_stop.set_enabled(False)
            self.action_open_preview.set_enabled(False)

    def on_start(self, *args):
        if not self.site:
            return
        self.site.server.start()
        self.on_activate()

    def on_stop(self, *args):
        if not self.site:
            return
        self.site.server.stop()
        self.on_activate()

    def on_restart(self, *args):
        if not self.site:
            return
        self.site.server.restart()
        self.on_activate()

    def on_open_preview(self, *args):
        if not self.site:
            return
        Gio.AppInfo.launch_default_for_uri(self.site.server.get_address())
        self.popdown()
