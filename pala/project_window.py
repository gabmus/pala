from gi.repository import Gtk, Adw, Gio, GLib
from pala.confManager import ConfManager
from pala.editor_view import EditorView
from pala.hugo import HugoSite
from pala.item_list_store import ItemObj
from pala.item_listbox_row import ItemListboxRow
from pala.action_helper import new_action, new_action_group
from pala.server_controller_button import ServerControllerButton
from pala.project_config_win import ProjectConfigWin


@Gtk.Template(resource_path='/org/gabmus/pala/ui/project_window.ui')
class ProjectWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'ProjectWindow'
    editor_view = Gtk.Template.Child()
    items_listbox = Gtk.Template.Child()
    new_item_name_entry = Gtk.Template.Child()
    new_item_create_btn = Gtk.Template.Child()
    new_menu_btn = Gtk.Template.Child()
    left_title_label = Gtk.Template.Child()
    leaflet = Gtk.Template.Child()
    headerbar = Gtk.Template.Child()
    server_controller_btn = Gtk.Template.Child()

    def __init__(self, site: HugoSite):
        super().__init__()
        self.confman = ConfManager()
        self.site = site
        self.server_controller_btn.set_site(self.site)
        self.items_listbox.bind_model(
            self.site.items_store, self.__create_row, None
        )
        self.left_title_label.set_text(self.site.name)
        self.on_leaflet_folded()
        self.editor_view.back_btn.connect(
            'clicked',
            lambda *args: self.leaflet.navigate(Adw.NavigationDirection.BACK)
        )

        self.connect('close-request', self.on_destroy)

        # actions
        new_action_group(self, 'proj', [
            new_action('show_config', self.show_config),
            new_action('show_in_file_manager', self.show_in_file_manager)
        ])

    def show_config(self, *args):
        config_win = ProjectConfigWin(self.site)
        config_win.set_transient_for(self)
        config_win.set_modal(True)
        config_win.present()

    def show_in_file_manager(self, *args):
        Gio.AppInfo.launch_default_for_uri(
            GLib.filename_to_uri(str(self.site.path))
        )

    def __create_row(self, item: ItemObj, *args) -> ItemListboxRow:
        row = ItemListboxRow(item)
        return row

    @Gtk.Template.Callback()
    def on_new_item_create_btn_clicked(self, *args):
        item_name = self.new_item_name_entry.get_text().strip()
        if not item_name:
            return
        self.site.create_item(item_name)
        self.new_menu_btn.popdown()

    @Gtk.Template.Callback()
    def on_new_item_name_entry_changed(self, *args):
        self.new_item_create_btn.set_sensitive(
            self.new_item_name_entry.get_text().strip()
        )

    @Gtk.Template.Callback()
    def on_items_listbox_row_activated(self, lb, row):
        self.editor_view.open_item(row.item, self.site)
        self.leaflet.navigate(Adw.NavigationDirection.FORWARD)

    @Gtk.Template.Callback()
    def on_leaflet_folded(self, *args):
        if self.leaflet.get_folded():
            self.editor_view.set_show_back_btn(True)
            if self.confman.wm_decoration_on_left:
                self.headerbar.set_show_start_title_buttons(True)
                self.editor_view.headerbar.set_show_start_title_buttons(True)
            else:
                self.headerbar.set_show_end_title_buttons(True)
                self.editor_view.headerbar.set_show_end_title_buttons(True)
        else:
            self.editor_view.set_show_back_btn(False)
            if self.confman.wm_decoration_on_left:
                self.headerbar.set_show_start_title_buttons(True)
                self.editor_view.headerbar.set_show_start_title_buttons(False)
            else:
                self.headerbar.set_show_end_title_buttons(False)
                self.editor_view.headerbar.set_show_end_title_buttons(True)

    def present(self):
        super().present()
        winsize = self.confman.conf['projects'][str(self.site.path)].get(
            'windowsize', {
                'width': 450, 'height': 400
            }
        )
        self.set_default_size(
            winsize.get('width', 450), winsize.get('height', 400)
        )

    def on_destroy(self, *args):
        self.site.server.stop()
        self.confman.conf['projects'][str(self.site.path)]['windowsize'] = {
            'width': self.get_width(), 'height': self.get_height()
        }
        self.confman.save_conf()
